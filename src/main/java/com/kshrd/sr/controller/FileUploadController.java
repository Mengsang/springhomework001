package com.kshrd.sr.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller

public class FileUploadController {
	
	String serverPath="C:/Users/ACER/Documents/FileUpload/";

	@GetMapping("/upload")
	public String upload() {
		return "upload";
	}
	
	@PostMapping("/upload")
	public String saveFile(@RequestParam("files") List<MultipartFile> files){
//		System.out.println(file.getOriginalFilename());
		
		if(!files.isEmpty()) {
			
		files.forEach(file -> {
			
			String fileName = UUID.randomUUID().toString() + file.getOriginalFilename();
			try {
				Files.copy(file.getInputStream(), Paths.get(serverPath,fileName));
			} catch (IOException e) {
			
				e.printStackTrace();
			}
			
		});
			
			
			//------
		}
		return "redirect:/upload" ;
	}
}
