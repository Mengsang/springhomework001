package com.kshrd.sr.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.kshrd.sr.model.Article;
import com.kshrd.sr.service.ArticleService;



@Controller
public class ArticleController {
	
	@Autowired
	private ArticleService articleService;
	
	String serverPath="C:/Users/ACER/Documents/FileUpload/";
	
	@GetMapping("/article")
	public String article(ModelMap m) {
		List<Article>articles= articleService.findAll();
		System.out.println(articles);
		m.addAttribute("articles",articles);
		return "article";
	}
	@GetMapping("/add")
	public String add(ModelMap m) {
		m.addAttribute("article", new Article());
		m.addAttribute("formAdd", true);
		return "add";
	}
	@PostMapping("/add")
	public String saveArticle(@RequestParam("image") MultipartFile thumbnail,@Valid @ModelAttribute Article article,BindingResult result,ModelMap m) 
	{
		if(result.hasErrors()) {
			m.addAttribute("article", article);
			m.addAttribute("formAdd", true);
			return "add";
		}
		//-----------------
		if(thumbnail.isEmpty()) {
			System.out.println("File Empty");
			return "add";
		}else {
			//-----------------add thumbnail image with new name
			String thumbnail1 = UUID.randomUUID().toString() + thumbnail.getOriginalFilename();
			//-----------------------
			try {
				Files.copy(thumbnail.getInputStream(), Paths.get(serverPath, thumbnail1));
			} catch (IOException e) {
				e.printStackTrace();
			}
			} 
		//-------------
		article.setThumbnail("/image/"+thumbnail.getOriginalFilename());
		
		//--------------
		article.setCreatedDate(new Date().toString());
		
		articleService.add(article);
		System.out.println(article);
		return "redirect:/article ";
	}
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id")int id) {
		articleService.delete(id);
		return "redirect:/article";
	}
	
	@GetMapping("/update/{id}")
	public String update(ModelMap m, @PathVariable int id) {
		m.addAttribute("article", articleService.findOne(id));
		m.addAttribute("formAdd", false);
		return "add";
	}
	
	@PostMapping("/update")
	public String saveUpdate(@ModelAttribute Article article) {
		article.setCreatedDate(new Date().toString());
		System.out.println(article);
		articleService.update(article);
		return "redirect:/article ";
}
}




