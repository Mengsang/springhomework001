package com.kshrd.sr.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class Article {
	@NotNull
	private int id;
	@NotBlank
	private String title;
	@NotBlank
	private String description;
	@NotBlank
	private String author;
	private String thumbnail;
	private String createdDate;

	

	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + ", description=" + description + ", author=" + author
				+ ", thumbnail=" + thumbnail + ", createdDate=" + createdDate + "]";
	}



	public Article(int id, String title, String description, String author, String thumbnail, String createdDate) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.author = author;
		this.thumbnail = thumbnail;
		this.createdDate = createdDate;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getAuthor() {
		return author;
	}



	public void setAuthor(String author) {
		this.author = author;
	}



	public String getThumbnail() {
		return thumbnail;
	}



	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}



	public String getCreatedDate() {
		return createdDate;
	}



	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}



	public Article() {}
}
