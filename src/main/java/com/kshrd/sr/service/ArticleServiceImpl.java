package com.kshrd.sr.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kshrd.sr.model.Article;
import com.kshrd.sr.repository.ArticleRepository;

@Service
public class ArticleServiceImpl implements ArticleService {
	
	@Autowired
	private ArticleRepository articleRepo;	
	
	@Override
	public void add(Article article) {
		articleRepo.add(article);
		
	}
	@Override
	public void delete(int id) {
		articleRepo.delete(id);
	}
	@Override
	public void update(Article article) {
		articleRepo.update(article);
		
	}

	@Override 
	public Article findOne(int id) {
		return articleRepo.findOne(id);
	}

	@Override
	public List<Article> findAll() {
		return articleRepo.findAll();
	}
	

}
