package com.kshrd.sr.service;

import java.util.List;

import com.kshrd.sr.model.Article;

public interface ArticleService {
	void add(Article article);
	Article findOne(int id);
	List<Article> findAll();
	void delete(int id);
	void update(Article article );
}
