package com.kshrd.sr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArticleManagementSystemSrApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArticleManagementSystemSrApplication.class, args);
	}
}
