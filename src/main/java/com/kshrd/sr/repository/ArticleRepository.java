package com.kshrd.sr.repository;

import java.util.List;

import com.kshrd.sr.model.Article;

public interface ArticleRepository {
	
	void add(Article article);
	Article findOne(int id);
	List<Article> findAll();
	
	void delete(int id);
	void update(Article article);
}
  